import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminComponent} from './admin-main/admin.component';
import {AdminDetailComponent} from './admin-detail/admin-detail.component';
import {LoginComponent} from './login/login.component';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'logowanie',
        component: LoginComponent
      },
      {
        path: ':slug',
        component: AdminDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
