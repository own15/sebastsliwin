import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public activeMenu: boolean;
  public activeDrop: boolean;

  constructor(public route: ActivatedRoute) {
  }

  ngOnInit(): void {

  }

  activeDropdown() {
    this.activeDrop = !this.activeDrop;
  }

  logout() {
  }


}
