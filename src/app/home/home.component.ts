import {Component, OnInit} from '@angular/core';
import {SwiperOptions} from 'swiper';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  config: SwiperOptions = {
    pagination: {el: '.swiper-pagination', clickable: true},
    slidesPerView: 1,
    speed: 1,
    loop: true,
  };

  // FORM
  contactForm: FormGroup;

  timeline = [
    {
      author: 'lorem ipsum',
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book.`,
      img: '/assets/images/logo.png',
      src: 'https://irlaser.pl'
    }
  ];

  constructor(
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  sendMessage() {
    const contactForm = {...this.contactForm.value};
    console.log(contactForm);
  }

}
