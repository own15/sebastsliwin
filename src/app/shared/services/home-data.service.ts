import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeDataService {
  apiURL = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAbout() {
    const url = `${this.apiURL}/about`;
    return this.http.get(url);
  }

  getServices() {
    const url = `${this.apiURL}/services`;
    return this.http.get(url);
  }

  getOpinion() {
    const url = `${this.apiURL}/opinion`;
    return this.http.get(url);
  }
}
