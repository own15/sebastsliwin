export const environment = {
  baseUrl: 'https://sebastiansliwinski.pl',
  API_URL: 'https://api.sebastiansliwinski.pl/',
  production: true
};
