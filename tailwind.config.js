module.exports = {
  theme: {
    extend: {},
    container: {
      center: true,
    },
    screens: {
      xs: '500px',
      sm: '640px',
      md: '768px',
      lg: '992px',
      xl: '1366px',
    },
    colors: {
      white: '#FFFFFF',
      black: '#000000',
      blue: '#0f4c81',
      darkwhite: '#fffcf5',
      lightblack: '#373838',
      admin: '#2a4053',
      adminBgc: '#eeeeee'
    },
    maxWidth: {
      '1/2': '50%',
      '1/3': '33.333333%',
      '1/4': '25%',
      '3/4': '75%',
    },
  },
  variants: {},
  plugins: [],
};
